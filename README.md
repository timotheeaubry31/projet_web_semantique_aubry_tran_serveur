
# Mét & Hauts (Serveur)

Une application de conseils vestimentaires qui utilise les données météo pour donner des recommandations sur les vêtements à porter pour la journée. L'application peut afficher les prévisions météo pour la journée, y compris les températures, la pluie et la neige prévues ou le vent. En fonction de ces informations, l'application peut donner des recommandations sur les vêtements à porter, tels que des vestes, des manteaux, des parapluies ou des bottes.


## Lancer en local

Cloner le project

```bash
  git clone https://gitlab.com/timotheeaubry31/projet_web_semantique_aubry_tran_serveur.git
```

Aller dans le répertoire du project

```bash
  cd projet_web_semantique_aubry_tran_serveur/apache-jena-fuseki-4.7.0/
```

Lancer l'application

```bash
  ./fuseki-server
```

Afficher la page

```http
  http://localhost:3030
```


## Technologies

**Serveur :** Apache Jena Fuseki

