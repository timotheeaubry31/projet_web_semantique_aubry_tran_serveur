import pandas as pd
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import FOAF

df = pd.read_csv('forecast.csv', delimiter = ",")

graph = Graph()

MYNS = Namespace('http://www.semanticweb.org/adminetu/ontologies/2023/4/untitled-ontology-2#')
graph.bind('myont', MYNS)
graph.bind('foaf', FOAF)

for index, row in df.iterrows():
    uri = MYNS[f"individual_{index + 1}"]
    graph.add((uri, RDF.type, MYNS.Toulouse))
    for column_name, value in row.items():
        cnames = column_name.split(';')
        values = str(value).split(';')
        for i in range(0, len(cnames)):
            if cnames[i] == "time" :
                    property_name = MYNS["aUneDate"]
                    graph.add((uri, property_name, Literal(values[i])))
            elif cnames[i] == "temperature_2m (°C)" :
                    property_name = MYNS["aUneTemperature"]
                    graph.add((uri, property_name, Literal(values[i])))
            elif cnames[i] == "windspeed_10m (km/h)" :
                    property_name = MYNS["aUneVitesseDeVent"]
                    graph.add((uri, property_name, Literal(values[i])))
            elif cnames[i] == "precipitation (mm)" :
                    property_name = MYNS["aUnTauxDePrecipitation"]
                    graph.add((uri, property_name, Literal(values[i])))
            elif cnames[i] == "cloudcover (%)" :
                    property_name = MYNS["aUnTauxEnsoleillement"]
                    graph.add((uri, property_name, Literal(values[i])))
graph.serialize(destination='output.ttl', format='turtle')